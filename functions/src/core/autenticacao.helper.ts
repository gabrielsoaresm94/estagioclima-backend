import * as admin from 'firebase-admin';
import {RequestFunctionPromise} from './interfaces.type';
import {asyncMiddleware} from './core.helper';
import {ErroSistema, RequisitionError, TiposErro} from '../shared/models/requisition-error.model';
import * as HttpStatus from 'http-status-codes';

/**
 *
 * callbackUsuarioAutenticado
 *
 * Callback que valida se um Firebase ID Token passado no request por autorização HTTP é válido.
 * Este metodo utiliza a API do Express e o Bearer token da forma:
 * `Authorization: Bearer <Firebase ID Token>`.
 * Quando decodificado o ID será acessível como req.user.
 * @param auth Objeto de autenticação passado por parâmetro
 */
export const callbackUsuarioAutenticado = (auth: admin.auth.Auth): RequestFunctionPromise => {
    return asyncMiddleware(async (req, res, next) => {

        // Mensagem de erro
        const mensagem_erro =
            new RequisitionError(
                new ErroSistema(
                    TiposErro.ERRO_NECESSARIO,
                    'Unauthorized'
                ),
                false
            );

        // Checa se a requisição tem os requisitos minimos de usuario
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
            console.error('[ERROR] {Autenticacao} Requisição não contém JWT válido');

            // Envia mensagem com erro informado
            res.status(HttpStatus.UNAUTHORIZED)
                .send(mensagem_erro)
                .end();
            return;
        }

        // Requisita o token desejado
        const idToken = req.headers.authorization.split('Bearer ')[1];

        // Decodifica o token e verifica se o mesmo é válido, envia este como parâmetro para próxima função
        try {
            req['user'] = await auth.verifyIdToken(idToken);
            next();
        } catch (err) {
            console.error('[ERROR] {Autenticacao} Token Inválido: ' + err);

            res.status(HttpStatus.UNAUTHORIZED)
                .send(mensagem_erro)
                .end();
        }
    });
};
