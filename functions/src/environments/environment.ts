import * as functions from 'firebase-functions';
import * as fs from 'fs';

/**
 * Seleciona o arquivo de environments de acordo com o ambiente atual
 */
export class Environment {

    /**
     * Pega as variaveis de ambiente do ambiente atual.
     * Não esquecer de salvar a variavel de ambiente com o comando:
     * firebase functions:config:set estagio_clima.node_env="prod"
     *
     * No caso de testar o ambiente local utilizar o comando:
     * firebase functions:config:get > .runtimeconfig.json
     */
    static getEnvironmentVariables(): EnviromentModel {
        const env_vars = functions.config().estagio_clima;

        // Verifica se existe o ambiente
        if (env_vars === undefined) {
            throw new Error('Variaveis de ambiente não configuradas. Configurar para liberar acesso!');
        }

        // Pega o ambiente do node a ser executado
        const env_name = env_vars.node_env;

        // Verifica se existe configuração para a variavel de ambiente
        if (env_name === undefined) {
            throw new Error('Variavel de ambiente node_env não configurada. Configurar para liberar acesso!');
        }

        // Path do endereço
        const env_path = `${__dirname}/environment.${env_name}.js`;

        // Verifica se existe o arquivo de variavel de ambiente
        if (!fs.existsSync(env_path)) {
            throw new Error('Environment é invalido ou nao existe: ' + env_name + ' ' + env_path);
        }

        // Retorna o arquivo carregado no ambiente pós compilação
        return require(env_path).environment;
    }

}

/**
 * Modelo para arquivos de variaveis de ambiente.
 */
export class EnviromentModel {

    /**
     * Configurações utilizadas pelo firebase
     */
    firebase: {
        /**
         * Especifica o arquivo de certificado do firebase a ser utilizado.
         */
        cert_file: string;

        /**
         * URl para o banco de dados a ser utilizado.
         */
        database_url: string;

        /**
         * Escolhe a instância do database a ser selecionada para os acionadores externos
         */
        database_instance: string;

        /**
         * UID que as CF irão utilizar para acessar o banco.
         */
        server_uid: string;
        /**
         * URl para o armazenamento de arquivos
         */
        storage_bucket: string;
    };
    /**
     * Regras de concessão de cors para cada ambiente.
     */
    cors: {
        /**
         * Vetor com as origens aceitas para as CloudFunctions.
         */
        origens: Array<string>;

    };
}
