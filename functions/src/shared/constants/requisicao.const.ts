/**
 * Constantes para requisições
 */
export class ConstantesRequisicao {
    public static readonly HTTP_TIPO_JSON = 'application/json';
}