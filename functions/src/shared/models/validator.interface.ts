/**
 * Interface para validação de modelos.
 */
export interface ValidatorInterface {
    validate(): boolean;
}